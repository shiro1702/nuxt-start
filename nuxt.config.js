module.exports = {
  // router: {
  //   middleware: 'check-auth'
  // },
  /*
  ** Headers of the page
  */
  head: {
    title: 'nuxt-start',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: 'Nuxt.js project' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ]
  },
  /*
  ** Customize the progress bar color
  */
  loading: { color: '#3B8070' },
  /*
  ** Build configuration
  */
  build: {
    /*
    ** Run ESLint on save
    */
    extend (config, { isDev, isClient }) {
      if (isDev && isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
      }
    },
    vendor: ['firebase']
  },
  // Buefy
  modules: [
      // Simple usage
      'nuxt-buefy',
      // Or you can customize
      ['nuxt-buefy', { 
        css: false, 
        materialDesignIcons: false}],
  ],
  plugins: [
    { src: '~plugins/VueQuillEditor.js', ssr: false },
    '~/plugins/firebase.js'
  ],
  css: [
    'quill/dist/quill.snow.css',
    'quill/dist/quill.bubble.css',
    'quill/dist/quill.core.css'
  ],
}

