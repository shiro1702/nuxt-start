// Базовый шаблон для инпутов
export const basicInput = {
    type: 'input',
    value: '',
    message: '',
    maxlength: 30,
    placeholder:'',
    icon: '',
    class: '',
    rounded: false, // скругление поля ввода
    disabled: false,
    loading: false, // иконки загрузки с права
};

export const email = {
    ...basicInput,
    label: 'Email',
    type: 'email',
    name: 'email',
    placeholder:'my@email.com',
};

export const pass = {
    ...basicInput,
    label: 'Pass',
    type: 'password',
    name: 'pass',
};


export const consent = {
    label: 'Согласие на обработку персональных данных',
    type: 'checkbox',
    name: 'checkbox',
    value: true,
    message: '',
}