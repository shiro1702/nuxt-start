import  {db} from '../plugins/firebase'
// import firebase from 'firebase/app'

export const state = () => ({
    item: {},
    items: [],
    itemsCount: 5,
})
export const getters = {
    item (state) {
        return state.item
    },
}

export const mutations = {
    addItems (state, data) {
        state.items = data
    },
    clearItems (state) {
        state.items = []
    },
    setItemsCount (state, data) {
        state.itemsCount = data;
    },
    addItem (state, data) {
        state.item = data
    },
}

export const actions = {
    LoadItem ({commit}, data) {
        return new Promise((resolve, reject) => {
            // let url = process.env.NODE_ENV !== 'production'
            let docRef = db.collection("blog").doc(data);
            docRef.get().then(function(doc) {
                if (doc.exists) {
                    commit('addItem',  doc.data());
                    resolve();
                } else {
                    // doc.data() will be undefined in this case
                    console.log("No such document!");
                    reject("No such document!");
                }
            }).catch(function(error) {
                // console.log("Error getting document:", error);
                reject(error);
            });
        })
    },
    LoadItems ({commit, state}, data) {
        // let url = process.env.NODE_ENV !== 'production'
        // console.log(process.env.NODE_ENV );
        // commit('addItems', data)
        return new Promise((resolve, reject) => {
            let startItem = null
            let pageCount = 3;

            let first = db.collection("blog")
                .orderBy("date", "desc")
                

            first.get().then( (documentSnapshots) => {
                // задать ограничения для пагинации
                commit( 'setItemsCount', parseInt(1 + documentSnapshots.docs.length/pageCount));

                startItem = documentSnapshots.docs[pageCount*(data.currentPage - 1)];

                let itemData = []
                documentSnapshots
                    .docs
                    .slice(parseInt(pageCount*(data.currentPage - 1)), parseInt( pageCount*(data.currentPage)))
                    .some((docItem) => {
                        if (docItem){
                            let item = {
                                id : docItem.id,
                            }
                            item = Object.assign({}, item, docItem.data());
                            itemData.push(item);  
                                
                            return false
                        } else {
                            return true
                        }
                    });

                // console.log('itemData',itemData)    
                commit('clearItems');
                commit('addItems', itemData);
                resolve();

                // if (startItem){
                //     let ref = db.collection("blog")
                //         .orderBy("date", "desc")
                //         .startAt(startItem)
                //         .limit(pageCount);
                //     //         // .orderBy("date")
                //     //         .orderBy("id")
                //     //         .startAt(startItem.id)
                //     //         .limit(3);
    
                //     ref.get()
                //         .then((req)=>{
                //             let itemData = []
                //             req.forEach(function(docItem) {
                //                 let item = {
                //                     id : docItem.id,
                //                 }
                //                 item = Object.assign({}, item, docItem.data());
                //                 itemData.push(item);
                //             });
                            
                //             // console.log('itemData',itemData)
                            
                //             commit('clearItems');
                //             commit('addItems', itemData);
                //             resolve();
                //         }).catch(function(error) {
                //             // console.log("Error getting document:", error);
                //             reject(error);
                //         });
                // }

            });
        })

    },
    CreateBlogArticle({commit}, data){
        // Add a new document in collection "cities"
        
        return new Promise((resolve, reject) => {
            // data.dateExample = firebase.firestore.Timestamp.fromDate(new Date("December 10, 1815")),
            db.collection("blog").doc().set(data)
                .then(function() {
                    console.log("Document successfully written!");
                    resolve();
                })
                .catch(function(error) {
                    console.error("Error writing document: ", error);
                    reject();
                });
        })
    }
}