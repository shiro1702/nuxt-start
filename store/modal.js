export const state = () => ({
    currentModal: ''
})

export const mutations = {
    SetCurrentModal (state, componentName) {
        state.currentModal = componentName;
    },
}

export const actions = {
    SetModal ({commit}, componentName) {
        commit('SetCurrentModal',componentName);
    },
    CloseModal({commit}){
        commit('SetCurrentModal', '');
    }
}