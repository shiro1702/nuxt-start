import firebase from 'firebase'

export const state = () => ({
    user: null
})

export const mutations = {
    setUser(state, user) {
        state.user = user ;
    },
}

export const actions = {
    LoadUser ({commit}, data) {
        return new Promise((resolve, reject) => {
            firebase.auth().onAuthStateChanged(function(user) {
                let userInfo = {};
                if (user){
                    userInfo.name = user.displayName
                    userInfo.email = user.email
                    userInfo.emailVerified = user.emailVerified
                    userInfo.metadata = user.metadata
                    userInfo.phoneNumber = user.phoneNumber
                    userInfo.image = user.photoURL
                    userInfo.providerData = user.providerData
                    userInfo.uid = user.uid
                    commit('setUser', userInfo);
                }
                resolve()
              });
        })
    },
    CreateUserByEmail ({commit}, data) {
        return new Promise((resolve, reject) => {
            firebase.auth().createUserWithEmailAndPassword(data.email, data.password).then(
                (data)=>{
                    console.log(data);
                    // user.additionalUserInfo: {
                    //     isNewUser: true // прверка зарегистрирован пользователь или нет
                    //     providerId: "password"
                    // }
                    let user = data.user
                    let profile = {}
                    profile.name = user.displayName;
                    profile.email = user.email;
                    profile.image = user.photoURL;
                    profile.emailVerified = user.emailVerified;
                    profile.id = user.uid;  // The user's ID, unique to the Firebase project. Do NOT use
                                    // this value to authenticate with your backend server, if
                                    // you have one. Use User.getToken() instead.
                    commit('setUser', profile);
                    resolve()
                }
            ).catch(function(error) {
                console.log(error.code);
                console.log(error.message);
                reject()
            });
        });
    },
    LoginUserByEmail({commit}, data){
        return new Promise((resolve, reject) => {
            firebase.auth().signInWithEmailAndPassword(data.email, data.password).then(
                (data)=>{
                    let user = data.user
                    let profile = {}
                    profile.name = user.displayName;
                    profile.email = user.email;
                    profile.image = user.photoURL;
                    profile.emailVerified = user.emailVerified;
                    profile.id = user.uid;  // The user's ID, unique to the Firebase project. Do NOT use
                                    // this value to authenticate with your backend server, if
                                    // you have one. Use User.getToken() instead.



                    // let user = data.users[0]
                    // let profile = {}
                    // profile.name = user.name;
                    // profile.email = user.email;
                    // profile.image = user.photoURL;
                    // profile.emailVerified = user.emailVerified;
                    // profile.id = user.localId;  // The user's ID, unique to the Firebase project. Do NOT use
                    //                 // this value to authenticate with your backend server, if
                    //                 // you have one. Use User.getToken() instead.
                    commit('setUser', profile);
                    resolve()
                }
            ).catch(function(error) {
                console.log(error.code);
                console.log(error.message);
                reject()
            });
        });
    },
    LogOut({commit}){
        firebase.auth().signOut().then(function() {
            // Sign-out successful.
            commit('setUser', null);
        }).catch(function(error) {
            // An error happened.
        });
    },
}