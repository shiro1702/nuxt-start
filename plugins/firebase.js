import firebase from 'firebase/app'
import 'firebase/firestore'
if (!firebase.apps.length) {
    const config = {
        apiKey: "AIzaSyB3kU8MN1s1WlDRRkwcmuCfVEtIanAOlus",
        authDomain: "nuxt-start-3c495.firebaseapp.com",
        databaseURL: "https://nuxt-start-3c495.firebaseio.com",
        projectId: "nuxt-start-3c495",
        storageBucket: "nuxt-start-3c495.appspot.com",
        messagingSenderId: "403670129363",
        appId: "1:403670129363:web:323c61115bbed9a2"
    }
    firebase.initializeApp(config)
    // firebase.firestore().settings({timestampsInSnapshots: true})
}
const db = firebase.firestore()

export {db}